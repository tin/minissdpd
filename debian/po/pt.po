# Translation of minissdpd's debconf messages to European Portuguese
# Copyright (C) 2018 THE minissdpd'S COPYRIGHT HOLDER
# This file is distributed under the same license as the minissdpd package.
#
# Américo Monteiro <a_monteiro@gmx.com>, 2018 - 2019.
msgid ""
msgstr ""
"Project-Id-Version: minissdpd 1.5.20180223-6\n"
"Report-Msgid-Bugs-To: minissdpd@packages.debian.org\n"
"POT-Creation-Date: 2019-01-31 14:58+0800\n"
"PO-Revision-Date: 2019-02-17 11:34+0000\n"
"Last-Translator: Américo Monteiro <a_monteiro@gmx.com>\n"
"Language-Team: Portuguese <>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid "MiniSSDP daemon configuration"
msgstr "Configuração do daemon do MiniSSDP"

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"The MiniSSDP daemon is being installed (perhaps as a dependency for UPnP "
"support) but will not function correctly until it is configured."
msgstr ""
"O daemon do MiniSSDP está a ser instalado (talvez como uma dependência para "
"suporte a UPnP) mas não irá funcionar correctamente até ser configurado."

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"MiniSSDP is a daemon used by MiniUPnPc to speed up device discovery. For "
"security reasons, no out-of-box default configuration can be provided, so it "
"requires manual configuration."
msgstr ""
"O MiniSSDP é um daemon usado pelo MiniUPnPc para acelerar a descoberta de "
"dispositivos. Por razões de segurança, não pode ser disponibilizada uma "
"configuração predefinida \"fora-da-caixa\", por isso requer configuração "
"manual."

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"Alternatively you can simply override the recommendation and remove "
"MiniSSDP, or leave it unconfigured - it won't work, but MiniUPnPc (and UPnP "
"applications) will still function properly despite some performance loss."
msgstr ""
"Alternativamente você pode simplesmente pular esta recomendação e remover o "
"MiniSSDP ou deixá-lo desconfigurado - não irá funcionar, mas o MiniUPnPc (e "
"aplicações de UPnP) irão funcionar correctamente apesar de alguma perda de "
"performance."

#. Type: boolean
#. Description
#: ../minissdpd.templates:3001
msgid "Start the MiniSSDP daemon automatically?"
msgstr "Iniciar automaticamente o daemon MiniSSDP?"

#. Type: boolean
#. Description
#: ../minissdpd.templates:3001
msgid ""
"Choose this option if the MiniSSDP daemon should start automatically, now "
"and at boot time."
msgstr ""
"Escolha esta opção se o daemon MiniSSDP deverá arrancar automaticamente, "
"agora e durante o arranque da máquina."

#. Type: string
#. Description
#: ../minissdpd.templates:4001
msgid "Interfaces to listen on for UPnP queries:"
msgstr "Interfaces onde escutar por pedidos de UPnP:"

#. Type: string
#. Description
#: ../minissdpd.templates:4001
msgid ""
"The MiniSSDP daemon will listen for requests on one interface, and drop all "
"queries that do not come from the local network. Please enter the LAN "
"interfaces or IP addresses of those interfaces (in CIDR notation) that it "
"should listen on, separated by space."
msgstr ""
"O daemon MiniSSDP irá escutar pedidos em uma interface, e ignorar todos os "
"pedidos que não venham da rede local. Por favor indique as interfaces LAN ou "
"os endereços IP dessas interfaces (em notação CIDR) de onde ele deverá "
"escutar, separados por espaços."

#. Type: string
#. Description
#: ../minissdpd.templates:4001
msgid ""
"Interface names are highly preferred, and required if you plan to enable "
"IPv6 port forwarding."
msgstr ""
"Nomes são altamente preferíveis e necessários se planear em activar o "
"reencaminhamento de portos IPv6."

#. Type: boolean
#. Description
#: ../minissdpd.templates:5001
msgid "Enable IPv6 listening?"
msgstr "Activar a escuta de IPv6?"

#. Type: boolean
#. Description
#: ../minissdpd.templates:5001
msgid ""
"Please specify whether the MiniSSDP daemon should listen for IPv6 queries."
msgstr ""
"Por favor especifique se o daemon MiniSSDP deverá escutar pedidos IPv6."

#~ msgid ""
#~ "If you see this message but never manually install MiniSSDP, then most "
#~ "probably this package is automatically pulled as a recommendation for "
#~ "libminiupnpc, which is the UPnP support library for many applications."
#~ msgstr ""
#~ "Se está a ver esta mensagem mas nunca instalou manualmente o MiniSSDP, "
#~ "então o mais provável é este pacote ter sido puxado automaticamente como "
#~ "uma recomendação para ibminiupnpc a qual é a biblioteca de suporte a UPnP "
#~ "para muitas aplicações."
