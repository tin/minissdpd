# Translation of minissdpd debconf template to Swedish
# Copyright (C) 2019 Martin Bagge <brother@bsnet.se>
# This file is distributed under the same license as the minissdpd package.
#
# Martin Bagge <brother@bsnet.se>, 2019
msgid ""
msgstr ""
"Project-Id-Version: minissdpd\n"
"Report-Msgid-Bugs-To: minissdpd@packages.debian.org\n"
"POT-Creation-Date: 2019-01-31 14:58+0800\n"
"PO-Revision-Date: 2019-02-12 21:49+0100\n"
"Last-Translator: Martin Bagge / brother <brother@bsnet.se>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid "MiniSSDP daemon configuration"
msgstr "Inställningar för MiniSSDP-tjänsten"

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"The MiniSSDP daemon is being installed (perhaps as a dependency for UPnP "
"support) but will not function correctly until it is configured."
msgstr ""
"Tjänsten MiniSSDP installeras (kanske som beroende för UPnP-stöd) men kommer "
"inte fungera korrekt innan den fått inställningar angivna."

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"MiniSSDP is a daemon used by MiniUPnPc to speed up device discovery. For "
"security reasons, no out-of-box default configuration can be provided, so it "
"requires manual configuration."
msgstr ""
"MiniSSDP är en tjänst som används av MiniUPnPc för att snabba upp "
"upptäckandet av enheter. Av säkerhetsskäl finns ingen standardinställning så "
"allt behöver hanteras manuellt."

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"Alternatively you can simply override the recommendation and remove "
"MiniSSDP, or leave it unconfigured - it won't work, but MiniUPnPc (and UPnP "
"applications) will still function properly despite some performance loss."
msgstr ""
"Alternativt så kan du avvisa rekommendationen och ta bort MiniSSDP, eller "
"lämna den utan inställningar - då kommer den inte fungera men MiniUPnPc (och "
"UPnP-applikationer) kommer fortfarande fungera med viss prestandaförlust."

#. Type: boolean
#. Description
#: ../minissdpd.templates:3001
msgid "Start the MiniSSDP daemon automatically?"
msgstr "Starta MiniSSDP-tjänsten automatiskt?"

#. Type: boolean
#. Description
#: ../minissdpd.templates:3001
msgid ""
"Choose this option if the MiniSSDP daemon should start automatically, now "
"and at boot time."
msgstr ""
"Välj detta alternativ om MiniSSDP-tjänsten ska starta automatiskt - nu och "
"vid uppstart."

#. Type: string
#. Description
#: ../minissdpd.templates:4001
msgid "Interfaces to listen on for UPnP queries:"
msgstr "Gränssnitt att lyssna på UPnP-förfrågningar:"

#. Type: string
#. Description
#: ../minissdpd.templates:4001
msgid ""
"The MiniSSDP daemon will listen for requests on one interface, and drop all "
"queries that do not come from the local network. Please enter the LAN "
"interfaces or IP addresses of those interfaces (in CIDR notation) that it "
"should listen on, separated by space."
msgstr ""
"MiniSSDP-tjänsten kommer att lyssna på förfrågningar på ett gränssnitt och "
"slänga alla förfrågningar som inte kommer från det lokala nätverket. Ange "
"LAN-gränssnitt och IP-adresser (i CIDR) som tjänsten ska lyssna på."

#. Type: string
#. Description
#: ../minissdpd.templates:4001
msgid ""
"Interface names are highly preferred, and required if you plan to enable "
"IPv6 port forwarding."
msgstr ""
"Namn på gränssnitt är att föredra och krävs för att stödja portförmedling i "
"IPv6."

#. Type: boolean
#. Description
#: ../minissdpd.templates:5001
msgid "Enable IPv6 listening?"
msgstr "Lyssna på IPv6?"

#. Type: boolean
#. Description
#: ../minissdpd.templates:5001
msgid ""
"Please specify whether the MiniSSDP daemon should listen for IPv6 queries."
msgstr "Ange om MiniSSDP-tjänsten ska lyssna på förfrågningar på IPv6."
